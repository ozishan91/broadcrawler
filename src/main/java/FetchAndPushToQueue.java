import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class FetchAndPushToQueue implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(FetchAndPushToQueue.class);

    private Node<String> url;

    public FetchAndPushToQueue(Node<String> url){
        this.url = url;
    }

    public void run() {

        try {
            LOGGER.info("Going to fetch url: "+url.getData());
            URL urlObj = new URL(url.getData());
            Document doc = Jsoup.parse(urlObj, 120*1000);
            Elements links = doc.select("a[href]");
            List<Node<String>> children = new ArrayList<>();

            for(int i = 0; i < links.size(); i++){
                String tempUrl = links.get(i).absUrl("href");
                LOGGER.info("Got url: "+tempUrl+ "from main url: "+ url.getData());
                LOGGER.info("Going to check validity of url: "+tempUrl);
                if(isUrlValid(tempUrl)){
                    if(BroadCrawler.urlsMap.get(tempUrl) == null){
                        LOGGER.warn("Going to add the obtained valid url: " + tempUrl + "in the final urls queue");
                        children.add(new Node<>(tempUrl));
                        BroadCrawler.urlsMap.put(tempUrl, true);
                    }
                    else{
                        LOGGER.warn("This url: "+ tempUrl + "has already been added to the site map");
                    }
                }
                else{
                    LOGGER.warn("The url: "+ tempUrl + "obtained, is an external url");
                }
            }
            BroadCrawler.tempRoot.addChildren(children);
            children.forEach(node -> BroadCrawler.finalUrlsQueue.add(node));

        } catch (Exception e) {
            LOGGER.error("Error: " + e.getMessage() + " failed while fetching the url: " + url);
            e.printStackTrace();
        }
    }

    public boolean isUrlValid(String url) {
        Boolean isValid = true;
        if (url == null) {
            isValid = false;
            LOGGER.info("URL is null, returning false");
        } else {
            URI uri = null;
            try {
                uri = new URI(url);

                String domain = uri.getHost();
                LOGGER.error("Domain name returned from url: " + domain);
                if (!(domain.equals("redhat.com") || domain.equals("www.redhat.com"))) {
                    isValid = false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return isValid;
    }
}
