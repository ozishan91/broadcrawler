import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.*;

public class BroadCrawler {

    private static final Logger LOGGER = LoggerFactory.getLogger(BroadCrawler.class);

    public static Map<String, Boolean> urlsMap = new HashMap<String, Boolean>();

    public static BlockingQueue<Node<String>> finalUrlsQueue = new LinkedBlockingQueue<Node<String>>();

    public static Node<String> tempRoot;

    public static Node<String> root;

    ExecutorService executorService;

    String seedUrl;

    public BroadCrawler(String seedUrl, int threadPoolSize){
        this.seedUrl = seedUrl;
        this.executorService = Executors.newFixedThreadPool(threadPoolSize);
    }

    public Boolean runCrawlJob() {
        Boolean jobStatus = true;
        root = new Node<>(seedUrl);
        finalUrlsQueue.add(root);
        urlsMap.put(seedUrl, true);

        try {
            while (!finalUrlsQueue.isEmpty()) {
                Node currUrl = finalUrlsQueue.poll();
                tempRoot = currUrl;
                LOGGER.info("Popped url from the queue: "+ currUrl.getData());
                FetchAndPushToQueue consumerObj = new FetchAndPushToQueue(currUrl);
                executorService.execute(consumerObj);
                if(finalUrlsQueue.isEmpty()) {
                    executorService.awaitTermination(30, TimeUnit.SECONDS);
                }
                LOGGER.info("queue size: "+ finalUrlsQueue.size());
            }
            LOGGER.info("Execution completed");
        } catch (Exception e) {
            LOGGER.error("Error: " + e.getMessage() + " failed to run the url discovery job for seed url: " + seedUrl);
            jobStatus = false;
        }
        return jobStatus;
    }

    public static <T> void printSiteMap(Node<T> node, String appender) {
        System.out.println(appender + node.getData());
        node.getChildren().forEach(each ->  printSiteMap(each, appender + appender));
    }

    public static void main(String [] args){
        String seedUrl = args[0];
        int threadPoolSize = Integer.parseInt(args[1]);
        BroadCrawler broadCrawler = new BroadCrawler(seedUrl, threadPoolSize);
        Boolean status = broadCrawler.runCrawlJob();
        if(status){
            printSiteMap(root, "|___");
        }
    }
}